![ARCHITARVE logo](https://gitlab.gwdg.de/ARCHITRAVE/SADE/raw/e541e20999135a3eb11f99686a507f7649fc8105/templates/images/logo.svg "ARCHITRAVE logo")

ARCHITRAVE is a digital edition project funded by DFG and ANR. This German-French research project provides several previously unedited reports of German travelers to France from the period 1685-1723.

## About

[![pipeline status](https://gitlab.gwdg.de/ARCHITRAVE/build/badges/master/pipeline.svg)](https://gitlab.gwdg.de/ARCHITRAVE/build/commits/master)

This repository is a fork of [SADE/build](https://gitlab.gwdg.de/SADE/build) and has been modified for the [ARCHITRAVE website](https://architrave.eu). SADE (Scalable Architecture for Digital Editions) is a backend and frontend application for publishing scholarly editions in a digital medium. It's based on [eXist database](https://exist-db.org).

## How to Use
This repository is part of a continuous integration process that leads to a ready to use [debian package](https://ci.de.dariah.eu/packages/pool/snapshots/s/sade-architrave/) designed for Ubuntu 18.

## Local usage

Dependencies:
- Running ElasticSearch instance!
- Gitlab env in /opt/contentapi.env for gitlab markdown files

Cleanup, in case something is cached
```sh
ant -f architrave.xml artifact-cleanup
```

Build
```sh
ant -f architrave.xml
```

Start local eXist-database
```sh
./build/sade/bin/startup.sh
```

## Contact
Please contact via [contact form on our website](https://architrave.eu/contact.html).
